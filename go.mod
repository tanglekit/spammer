module gitlab.com/tanglekit/spammer

go 1.15

require (
	github.com/iotadevelopment/go v0.0.0-20190417025043-28d46cb4ce25
	github.com/iotaledger/iota.go v1.0.0-beta.15
	github.com/pelletier/go-toml v1.8.0 // indirect
	github.com/spf13/afero v1.3.4 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.7.1
	gitlab.com/powsrv.io/go/client v0.0.0-20200807151725-8bc5209c1820
	gitlab.com/tanglekit/go/logs v0.0.0-20190721161506-c7a10c1be208
	gitlab.com/tanglekit/go/nodelist v0.0.0-20200821082640-d90bc590577c
)
